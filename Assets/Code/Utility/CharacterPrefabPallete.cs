﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newCharacterPrefabPalette", menuName = "Palettes/Characters")]
public class CharacterPrefabPallete : ScriptableObject
{
    public PlayerCharacter PlayerCharacterPrefab;
}
