﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : Singleton<MapManager>
{
    [SerializeField] private PlayerController PlayerController;
    [SerializeField] private PlayerCharacter PlayerCharacter;

    //[SerializeField] private EditorLevelData EditorLevelData;

    public LevelGrid LevelGrid;

    public bool InputAvailable;

    [SerializeField] private bool turnCycleFinished;

    public BaseCharacter[] CharacterTurnOrder;


    // Start is called before the first frame update
    void Start()
    {
        if(PlayerController == null)
        {
            PlayerController = ScriptableObject.CreateInstance<PlayerController>();
            PlayerController.UpKey = KeyCode.UpArrow;
            PlayerController.DownKey = KeyCode.DownArrow;
            PlayerController.LeftKey = KeyCode.LeftArrow;
            PlayerController.RightKey = KeyCode.RightArrow;
        }

        if(LevelGrid == null)
        {
            LevelGrid = FindObjectOfType<LevelGrid>();
        }


        PlayerCharacter = FindObjectOfType<PlayerCharacter>();
        if (PlayerCharacter == null)
        {
            PlayerCharacter = Instantiate(GameManager.Instance.CharacterPrefabPallete.PlayerCharacterPrefab, null);
            if(LevelGrid != null)
            {
                PlayerCharacter.transform.position = new Vector3(LevelGrid.PlayerStartCoordinates.X, 0, LevelGrid.PlayerStartCoordinates.Y);
            }
        }

        CharacterTurnOrder = GetCharacterTurnOrder();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKey(KeyCode.M))
        //{
        //    LevelGrid.LoadLevel();
        //}
        if (PlayerController.PressedUp())
        {
            PlayerMovement(MoveDirection.UP);
            return;
        }
        if (PlayerController.PressedDown())
        {
            PlayerMovement(MoveDirection.DOWN);
            return;
        }
        if (PlayerController.PressedLeft())
        {
            PlayerMovement(MoveDirection.LEFT);
            return;
        }
        if (PlayerController.PressedRight())
        {
            PlayerMovement(MoveDirection.RIGHT);
            return;
        }

        //if (Input.GetKeyUp(KeyCode.M))
        //{
        //    if(EditorLevelData != null)
        //    {
        //        //LevelData.SaveLevelMap(LevelGrid.Tiles);
        //    }

        //}
    }
    private void PlayerMovement(MoveDirection moveDirection) 
    {
        PlayerCharacter.MoveDirection = moveDirection;
        InputAvailable = false;
        StopCoroutine(ProcessTurnRotation());
        StartCoroutine(ProcessTurnRotation());
    }
    public bool IsTurnCycleFinished()
    {
        return turnCycleFinished;
    }
    Func<bool> TurnCycleFinished()
    {
        return IsTurnCycleFinished;
    }

    public void ProcessCharacterTurn(BaseCharacter character)
    {
        PlayerCharacter.MoveCharacter();
    }
    IEnumerator ProcessTurnRotation()
    {
        for (int i = 0; i < CharacterTurnOrder.Length; i++)
        {
            CharacterTurnOrder[i].MoveCharacter();
        }
        turnCycleFinished = true;
        yield return new WaitUntil(TurnCycleFinished());
        InputAvailable = true;
    }

    private BaseCharacter[] GetCharacterTurnOrder()
    {
        BaseCharacter[] baseCharacters = FindObjectsOfType<BaseCharacter>();
        
        Stack<BaseCharacter> characterStack = new Stack<BaseCharacter>();

        for (int i = 0; i < baseCharacters.Length; i++)
        {

            if (baseCharacters[i] != null)
            {
                if (baseCharacters[i].IsDead)
                {
                    Destroy(baseCharacters[i]);
                }
                else
                {
                    characterStack.Push(baseCharacters[i]);
                }
            }
        }


        Stack<BaseCharacter> sortedStack = new Stack<BaseCharacter>();

        while (characterStack.Count != 0)
        {
            BaseCharacter temp = characterStack.Peek();
            characterStack.Pop();
            while (sortedStack.Count != 0 && sortedStack.Peek().Speed > temp.Speed)
            {
                characterStack.Push(sortedStack.Peek());
                sortedStack.Pop();
            }

            sortedStack.Push(temp);
        }
        PrintCharacterTurnOrder(sortedStack.ToArray());
        return sortedStack.ToArray();
    }
    private void CleanUpDeadCharacters()
    {

    }
    private void PrintCharacterTurnOrder(BaseCharacter[] characterTurnOrder)
    {
        Debug.Log("Printing Turn Order");
        Debug.Log(characterTurnOrder.Length);
        for (int i = 0; i < characterTurnOrder.Length; i++)
        { 

            if(characterTurnOrder[i] != null)
            {
                Debug.Log(JsonUtility.ToJson(characterTurnOrder[i]));
            }
            else
            {
                Debug.Log("Skipping Dead or Null Character");
            }
        }
    }
}
