﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveDirection { UP, DOWN, LEFT, RIGHT }