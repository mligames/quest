﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCharacter : MonoBehaviour
{
    public string CharacterName;
    public int Speed;
    public bool IsDead;
    public LevelTile CurrentTile;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        //UpdateCharacterLocation(MapManager.Instance.LevelGrid.GetTile(transform.position.x, transform.position.y));
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        
    }

    protected virtual void UpdateCharacterLocation(LevelTile gridTile)
    {
        CurrentTile = gridTile;
    }
    protected virtual void RotateCharacter(MoveDirection moveDirection)
    {
        switch (moveDirection)
        {
            case MoveDirection.UP:
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                break;
            case MoveDirection.DOWN:
                transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                break;
            case MoveDirection.LEFT:
                transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
                break;
            case MoveDirection.RIGHT:
                transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                break;
            default:
                break;
        }
    }
    public abstract void MoveCharacter();
}
