﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditorUI : Singleton<LevelEditorUI>
{
	public TileSelect[] TileSelections;
	public TileSelect ActiveTileSelected;

	public int SelectedFeature;

	public EditorTile PlayerStartTile;

	public TileInfoPanel TileInfoPanel;

	public Toggle PlayerStartPreReq;
	public bool IsMapValid;

	private void Start()
	{
		if(TileInfoPanel == null)
		{
			TileInfoPanel = GetComponentInChildren<TileInfoPanel>();
		}
		if(TileSelections.Length > 0)
		{
			for (int i = 0; i < TileSelections.Length; i++)
			{
				if(i == 0)
				{
					ActiveTileSelected = TileSelections[i];
				}

				TileSelections[i].OnTileSelect.AddListener(TileSelected);
			}

		}
		SelectFeature(0);
		ValidateMap();
	}

	public void TileSelected(TileSelect selectedTile)
	{
		ActiveTileSelected = selectedTile;
		
	}
	public void CreateWall(EditorTile tile)
	{
		WallData wallData = Instantiate(ActiveTileSelected.WallPrefab, tile.transform);
		wallData.transform.rotation = Quaternion.Euler(new Vector3(0, ActiveTileSelected.WallRotation, 0));
		wallData.WallType = ActiveTileSelected.WallType;
		wallData.YRotation = ActiveTileSelected.WallRotation;
		wallData.gameObject.name = "Wall_" + ActiveTileSelected.WallType.ToString() + "_R" + ActiveTileSelected.WallRotation;
		tile.TileType = TileType.WALL;

		if (tile.IsPlayerStart)
		{
			tile.IsPlayerStart = false;
			PlayerStartTile = null;
		}

		if (tile.IsEnemyStart)
		{
			tile.IsEnemyStart = false;
		}
	}
	public void SelectFeature(int index)
	{
		SelectedFeature = index;
	}
	public void ClearTileFeatures(EditorTile tile)
	{
		if (PlayerStartTile != null)
		{
			PlayerStartTile.IsPlayerStart = false;
			PlayerStartTile = null;
		}
		tile.ClearFeatures();
	}
	public void AddFeatureToTile(EditorTile tile)
	{
		if(SelectedFeature == 0)
		{
			PlayerStartTile = tile;
			tile.IsPlayerStart = true;
			//tile.IsEnemyStart = false;
		}

		if(SelectedFeature == 1)
		{
			//if (tile.IsPlayerStart) 
			//{
			//	tile.IsPlayerStart = false;
			//	PlayerStartTile = null;
			//}
			tile.IsEnemyStart = true;
		}
	}
	public void ValidateMap()
	{
		if (PlayerStartTile == null)
		{
			PlayerStartPreReq.isOn = false;
			IsMapValid = PlayerStartPreReq.isOn;
		}
		else
		{
			PlayerStartPreReq.isOn = true;
		}
		IsMapValid = PlayerStartPreReq.isOn;

		if (IsMapValid)
		{

		}
	}
	public void DisplayTileInfo(EditorTile tile)
	{
		TileInfoPanel.gameObject.SetActive(true);
		TileInfoPanel.DisplayTileInfo(tile);
	}
}
