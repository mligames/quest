﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallData : MonoBehaviour
{
    [SerializeField] private WallType wallType;
    public WallType WallType { get { return wallType; } set { wallType = value; } }

    [SerializeField] private float yRotation;
    public float YRotation { get { return yRotation; } set { yRotation = value; } }
}
