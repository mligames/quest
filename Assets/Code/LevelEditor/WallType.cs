﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WallType { NONE, BAR, CORNER, T, X }