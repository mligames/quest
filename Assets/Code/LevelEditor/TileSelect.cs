﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable] public class TileSelectEvent : UnityEvent<TileSelect> { }

[RequireComponent(typeof(Toggle))]
public class TileSelect : MonoBehaviour
{
    [SerializeField] private Toggle toggleComponent;

    public WallData WallPrefab;
    public WallType WallType;
    public float WallRotation;

    public TileSelectEvent OnTileSelect;

    // Start is called before the first frame update
    void Start()
    {
        if(toggleComponent != null)
        {
            toggleComponent.onValueChanged.AddListener(TileSelected);
        }
        else
        {
            toggleComponent = GetComponent<Toggle>();
        }

    }

    private void TileSelected(bool selected)
    {
        if (toggleComponent.isOn)
        {
            Debug.Log(this.name + " Selected " + toggleComponent.isOn);

        }
        OnTileSelect.Invoke(this);
    }
}
