﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorMap : Singleton<LevelEditorMap>
{
    [Header("LevelSettings")]
    public Vector2 LevelSize;
    public int TileScale; 
    public Dictionary<TileCoordinates, EditorTile> Tiles;

    [Header("Editor Tile Prefab")]
    public EditorTile EditorTilePrefab;

    [Header("Editor Save Data")]
    public EditorLevelData EditorLevelSavaData;

    // Start is called before the first frame update
    void Start()
    {
        Tiles = new Dictionary<TileCoordinates, EditorTile>();
        //if (EditorLevelSavaData != null)
        //{
        //    foreach (var item in EditorLevelSavaData.TileDataList)
        //    {
        //        TileCoordinates coordinates = item.coordinates;
        //        Tiles.Add(coordinates, CreateEditorTile(coordinates));
        //    }

        //}
        //else
        //{
        //    GenerateMap();
        //}
        GenerateMap();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateMap()
    {

        for (int x = 0; x < LevelSize.x; x++)
        {
            for (int y = 0; y < LevelSize.y; y++)
            {
                TileCoordinates coordinates = new TileCoordinates(x, y);
                Tiles.Add(coordinates, CreateEditorTile(coordinates));
            }
        }

    }
    public void LoadMap(LevelData levelSavaData)
    {
        if (EditorLevelSavaData == null)
        {
            Debug.Log("No Save Data");
            return;
        }
        this.LevelSize = levelSavaData.LevelSize;
        foreach (var item in levelSavaData.TileDataList)
        {

        }

    }
    public void SaveMap()
    {
        if(EditorLevelSavaData == null)
        {
            Debug.Log("No Save Data");
            return;
        }
        if (LevelEditorUI.Instance.IsMapValid)
        {
            EditorLevelSavaData.SaveLevelMap(Tiles);
        }
    }
    public EditorTile CreateEditorTile(TileCoordinates coordinates)
    {
        EditorTile editorTile = Instantiate(EditorTilePrefab);

        editorTile.gameObject.transform.position = new Vector3(coordinates.X, 0.0f, coordinates.Y) * TileScale;

        editorTile.gameObject.name = "Tile" + coordinates.ToString();

        editorTile.TileType = TileType.GROUND;
        editorTile.Coordinates = coordinates;

        editorTile.SetTileText(coordinates.ToString());
        return editorTile;
    }
    public EditorTile CreateEditorTile(TileData tileData)
    {
        EditorTile editorTile = Instantiate(EditorTilePrefab);
        editorTile.Coordinates = tileData.coordinates;
        editorTile.gameObject.transform.position = new Vector3(tileData.coordinates.X, 0.0f, tileData.coordinates.Y) * TileScale;
        editorTile.gameObject.name = "Tile" + tileData.coordinates.ToString();
        editorTile.TileType = tileData.tileType;
        editorTile.Coordinates = tileData.coordinates;
        editorTile.IsPlayerStart = tileData.isPlayerStart;
        editorTile.IsEnemyStart = tileData.isEnemyStart;

        if (tileData.isWall)
        {


        }


        editorTile.SetTileText(editorTile.Coordinates.ToString());
        return editorTile;
    }
    public EditorTile GetTile(TileCoordinates coordinates)
    {
        if (Tiles.TryGetValue(coordinates, out EditorTile editorTile))
        {
            return editorTile;
        }
        Debug.Log("Tile" + coordinates.ToString() + " Not Found");
        return null;
    }

    public EditorTile GetTile(float x, float y)
    {
        TileCoordinates coordinates = new TileCoordinates((int)x, (int)y);

        if (Tiles.TryGetValue(coordinates, out EditorTile editorTile))
        {
            return editorTile;
        }
        Debug.Log("Tile" + coordinates.ToString() + " Not Found");
        return null;
    }

}
