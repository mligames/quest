﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newLevel", menuName = "Level Design/New Level")]
public class EditorLevelData : ScriptableObject
{
    public Vector2 LevelSize;
    public List<EditorTileData> TileDataList;
    public void SaveLevelMap(Dictionary<TileCoordinates, EditorTile> tiles)
    {
        TileDataList = new List<EditorTileData>();
        foreach (KeyValuePair<TileCoordinates, EditorTile> kvp in tiles)
        {
            TileDataList.Add(kvp.Value.SaveData());
        }
    }
}
