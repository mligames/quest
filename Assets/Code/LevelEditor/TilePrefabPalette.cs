﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newTilePrefabPalette", menuName = "Palettes/Tiles")]
public class TilePrefabPalette : ScriptableObject
{
    public int TileScale = 2;
    public LevelTile GroundTilePrefab;
    [Header("Wall Prefabs")]
    public WallData WallBarPrefab;
    public WallData WallCornerPrefab;
    public WallData WallTPrefab;
    public WallData WallXPrefab;

    public LevelTile CreateGroundTile(TileCoordinates coordinates)
    {
        LevelTile gridTile = Instantiate(GroundTilePrefab, MapManager.Instance.transform);

        gridTile.gameObject.transform.position = new Vector3(coordinates.X, 0.0f, coordinates.Y) * TileScale;

        gridTile.gameObject.name = "Tile" + coordinates.ToString();

        gridTile.TileType = TileType.GROUND;
        gridTile.Coordinates = coordinates;

        return gridTile;
    }
    //public GridTile CreateWallTile(TileCoordinates coordinates)
    //{
    //    GridTile gridTile = Instantiate(WallTilePrefab, MapManager.Instance.transform);

    //    gridTile.gameObject.transform.position = new Vector3(coordinates.X, 0.0f, coordinates.Y) * TileScale;

    //    gridTile.gameObject.name = "Tile" + coordinates.ToString();
    //    gridTile.TileType = TileType.;
    //    gridTile.Coordinates = coordinates;

    //    gridTile.IsPassable = false;

    //    return gridTile;
    //}
    public void CreateWallTile(TileCoordinates coordinates, WallType wallType, float wallRotation)
    {


    }


}
