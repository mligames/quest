﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TileInfoPanel : MonoBehaviour
{
    public TMP_Text TileTypeText;
    public TMP_Text TileCoordinateText;
    public TMP_Text IsPlayerStartText;
    public TMP_Text IsEnemyStartText;
    public TMP_Text CanOccupyText;

    public void DisplayTileInfo(EditorTile tile)
    {
        TileTypeText.text = tile.TileType.ToString();
        TileCoordinateText.text = tile.Coordinates.ToString();
        if (tile.IsPlayerStart)
        {
            IsPlayerStartText.text = "PlayerStart YES";
        }
        else
        {
            IsPlayerStartText.text = "PlayerStart NO";
        }

        if (tile.IsEnemyStart)
        {
            IsEnemyStartText.text = "EnemyStart YES";
        }
        else
        {
            IsEnemyStartText.text = "EnemyStart NO";
        }

        if (tile.CanOccupy)
        {
            CanOccupyText.text = "CanOccupy YES";
        }
        else
        {
            CanOccupyText.text = "CanOccupy NO";
        }
    }
}
