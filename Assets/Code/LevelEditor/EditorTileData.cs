﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EditorTileData
{
    public TileType tileType;
    public TileCoordinates coordinates;
    public Vector3 worldLocation;
    public bool isWall;
    public WallType wallType;
    public float wallYRotation;
    public bool isPlayerStart;
    public bool isEnemyStart;
    public string characterData;

    public EditorTileData(TileCoordinates tileCoordinates, Vector3 tileworldLocation, TileType tileType, bool isWall, WallType wallType, float wallYRotation, bool isPlayerStart, bool isEnemyStart, string characterData)
    {
        coordinates = new TileCoordinates(tileCoordinates.X, tileCoordinates.Y);
        worldLocation = tileworldLocation;
        this.tileType = tileType;
        this.isWall = isWall;
        this.wallType = wallType;
        this.wallYRotation = wallYRotation;
        this.isPlayerStart = isPlayerStart;
        this.isEnemyStart = isEnemyStart;

        if (characterData == "" || characterData == null)
        {
            this.characterData = "NONE";
        }
        else
        {
            this.characterData = characterData;
        }

    }
    public override string ToString()
    {
        return "Tile" + coordinates.ToString();
    }
}
