﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newPlayerController", menuName = "PlayerController")]
public class PlayerController : ScriptableObject
{
    public KeyCode UpKey;
    public KeyCode DownKey;
    public KeyCode LeftKey;
    public KeyCode RightKey;
    private void OnEnable()
    {
        Debug.Log("PlayerControlSetting Enabled");
    }

    public bool PressedUp()
    {
        if (Input.GetKeyDown(UpKey))
        {
            return true;
        }
        return false;
    }
    public bool PressedDown()
    {
        if (Input.GetKeyDown(DownKey))
        {
            return true;
        }
        return false;
    }
    public bool PressedLeft()
    {
        if (Input.GetKeyDown(LeftKey))
        {
            return true;
        }
        return false;
    }
    public bool PressedRight()
    {
        if (Input.GetKeyDown(RightKey))
        {
            return true;
        }
        return false;
    }
}
