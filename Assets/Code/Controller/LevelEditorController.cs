﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Camera))]
public class LevelEditorController : MonoBehaviour
{
    private Camera debugCamera;
    public float LookSpeedH = 2f;
    public float LookSpeedV = 2f;
    public float ZoomSpeed = 2f;
    public float DragSpeed = 5f;

    private float Yaw;
    private float Pitch;

    public float RaycastRange;
    public LayerMask HitLayerMask;

    private void Start()
    {
        debugCamera = GetComponent<Camera>();
        Yaw = transform.eulerAngles.y;
        Pitch = transform.eulerAngles.x;
    }

    void Update()
    {
        if (!enabled) return;

        // Tile Data to Tile Info UI
        if (!EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.F) && !Input.GetKey(KeyCode.W))
        {

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, RaycastRange, HitLayerMask))
            {
                WorldToTileCoodinates(hit.point);

                EditorTile tile = LevelEditorMap.Instance.GetTile(WorldToTileCoodinates(hit.point));
                if (tile != null)
                {
                    LevelEditorUI.Instance.DisplayTileInfo(tile);
                }
            }
        }

        // Add Wall
        if (!EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.F) && Input.GetKey(KeyCode.W))
        {
                
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, RaycastRange, HitLayerMask))
            {
                WorldToTileCoodinates(hit.point);
                
                EditorTile tile = LevelEditorMap.Instance.GetTile(WorldToTileCoodinates(hit.point));
                if(tile != null)
                {
                    WallData wall = tile.GetComponentInChildren<WallData>();
                    if (wall != null)
                    {
                        wall.transform.parent = null;
                        Destroy(wall.gameObject);
                        tile.TileType = TileType.GROUND;
                    }
                    LevelEditorUI.Instance.CreateWall(tile);
                }
            }
            LevelEditorUI.Instance.ValidateMap();
        }
        // Remove Wall
        if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.F))
        {

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, RaycastRange, HitLayerMask))
            {
                WorldToTileCoodinates(hit.point);

                EditorTile tile = LevelEditorMap.Instance.GetTile(WorldToTileCoodinates(hit.point));           

                if (tile != null)
                {
                    WallData wall = tile.GetComponentInChildren<WallData>();
                    if (wall != null)
                    {
                        wall.transform.parent = null;
                        Destroy(wall.gameObject);
                        tile.TileType = TileType.GROUND;
                    }
                    tile.TileType = TileType.GROUND;
                    LevelEditorUI.Instance.DisplayTileInfo(tile);
                }
            }
            LevelEditorUI.Instance.ValidateMap();
        }

        // Add Feature
        if (Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.F))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, RaycastRange, HitLayerMask))
            {
                WorldToTileCoodinates(hit.point);

                EditorTile tile = LevelEditorMap.Instance.GetTile(WorldToTileCoodinates(hit.point));

                if (tile != null)
                {
                    WallData wall = tile.GetComponentInChildren<WallData>();
                    if (wall != null)
                    {
                        wall.transform.parent = null;
                        Destroy(wall.gameObject);
                        tile.TileType = TileType.GROUND;
                    }
                    LevelEditorUI.Instance.ClearTileFeatures(tile);
                    LevelEditorUI.Instance.AddFeatureToTile(tile);
                    LevelEditorUI.Instance.DisplayTileInfo(tile);
                }
            }
            LevelEditorUI.Instance.ValidateMap();
        }
        // Remove Features
        if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.F))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, RaycastRange, HitLayerMask))
            {
                WorldToTileCoodinates(hit.point);

                EditorTile tile = LevelEditorMap.Instance.GetTile(WorldToTileCoodinates(hit.point));

                if (tile != null)
                {
                    WallData wall = tile.GetComponentInChildren<WallData>();
                    if (wall != null)
                    {
                        wall.transform.parent = null;
                        Destroy(wall.gameObject);
                        tile.TileType = TileType.GROUND;
                    }

                    LevelEditorUI.Instance.ClearTileFeatures(tile);
                    LevelEditorUI.Instance.DisplayTileInfo(tile);
                }
            }
            LevelEditorUI.Instance.ValidateMap();
        }

        //Look around with Right Mouse
        if (Input.GetMouseButton(1))
        {
            Yaw += LookSpeedH * Input.GetAxis("Mouse X");
            Pitch -= LookSpeedV * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(Pitch, Yaw, 0f);

            Vector3 offset = Vector3.zero;
            float offsetDelta = Time.deltaTime * DragSpeed;
            if (Input.GetKey(KeyCode.LeftShift)) offsetDelta *= 5.0f;
            if (Input.GetKey(KeyCode.S)) offset.z -= offsetDelta;
            if (Input.GetKey(KeyCode.W)) offset.z += offsetDelta;
            if (Input.GetKey(KeyCode.A)) offset.x -= offsetDelta;
            if (Input.GetKey(KeyCode.D)) offset.x += offsetDelta;
            if (Input.GetKey(KeyCode.Q)) offset.y -= offsetDelta;
            if (Input.GetKey(KeyCode.E)) offset.y += offsetDelta;

            transform.Translate(offset, Space.Self);
        }

        //drag camera around with Middle Mouse
        if (Input.GetMouseButton(2))
        {
            transform.Translate(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * DragSpeed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * DragSpeed, 0);
        }

        //Zoom in and out with Mouse Wheel
        transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * ZoomSpeed, Space.Self);

    }

    private int FloatToGridValue(float value)
    {
        float result = Mathf.FloorToInt(value);
        float r = result % 2;
        if (r != 0)
        {
            result += 1;
        }
        result = (result / 2);

        return (int)result;
    }
    private TileCoordinates WorldToTileCoodinates(Vector3 worldPoint)
    {
        int x = FloatToGridValue(worldPoint.x);
        int z = FloatToGridValue(worldPoint.z);
        return new TileCoordinates(x, z);
    }
}
