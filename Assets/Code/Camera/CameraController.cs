﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{   
    public Transform Target;
    public CameraSetting CameraSetting;
    // Start is called before the first frame update
    private void Start()
    {
        if(Target == null)
        {
            Target = FindObjectOfType<PlayerCharacter>().transform;
        }
        transform.position = new Vector3(0, 0, 0);

        transform.parent = null;
        transform.Rotate(CameraSetting.tiltAngle);
    }
    private void LateUpdate()
    {
        transform.position = Target.position + CameraSetting.offset;
    }
}
