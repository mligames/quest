﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTile : MonoBehaviour
{
    [SerializeField] private TileType tileType;
    public TileType TileType { get { return tileType; } set { tileType = value; } }

    [SerializeField] private TileCoordinates coordinates;
    public TileCoordinates Coordinates { get { return coordinates; } set { coordinates = value; } }

    //[SerializeField] private Vector3 worldLocation;
    //public Vector3 WorldLocation { get { return worldLocation; } set { worldLocation = value; } }
    public Vector3 WorldLocation { get { return transform.position; } }
    public Vector3 WorldCenter { get { return new Vector3(transform.position.x, transform.position.y + 1, transform.position.z); } }

    [SerializeField] private BaseCharacter occupantCharacter;
    public BaseCharacter OccupantCharacter { get { return occupantCharacter; } set { occupantCharacter = value; } }

    public bool CanOccupy { get { if (tileType == TileType.GROUND) { return true; } else { return false; } } }

    [SerializeField] private bool isOccupied;
    public bool IsOccupied { get { return isOccupied; } set { isOccupied = value; } }

    [SerializeField] private bool isPlayerStart;
    public bool IsPlayerStart { get { return isPlayerStart; } set { isPlayerStart = value; } }

    [SerializeField] private bool isEnemyStart;
    public bool IsEnemyStart { get { return isEnemyStart; } set { isEnemyStart = value; } }

    private void Start()
    {
        
    }

    public void ClearFeatures()
    {
        isOccupied = false;
        occupantCharacter = null;
        IsPlayerStart = false;
        IsEnemyStart = false;
    }
    public TileData SaveData()
    {
        bool isWall = false;
        float wallRotation = 0;
        string characterData = "";
        WallType wallType = WallType.NONE;
        if (tileType == TileType.WALL)
        {
            isWall = true;
            WallData wall = GetComponentInChildren<WallData>();
            if (wall)
            {
                wallRotation = wall.YRotation;
            }
        }
        if (occupantCharacter != null)
        {
            characterData = JsonUtility.ToJson(occupantCharacter);
        }

        return new TileData(coordinates, transform.position, tileType, isWall, wallType, wallRotation, isPlayerStart, isEnemyStart, characterData);
    }
    public override string ToString()
    {
        return "Tile"+ coordinates.ToString();
    }
#if UNITY_EDITOR
    /// <summary>
    /// Draws the links between nodes for editor purposes
    /// </summary>
    protected virtual void OnDrawGizmos()
    {
        if (IsPlayerStart)
        {
            Gizmos.color = Color.green;
        }
        else if (IsEnemyStart)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.white;
        }

        Gizmos.DrawWireCube(new Vector3(transform.position.x, transform.position.y + 0.15f, transform.position.z), new Vector3(2, 0.25f, 2));
        //Gizmos.DrawWireSphere(WorldCenter, 0.25f);
    }
#endif

}
