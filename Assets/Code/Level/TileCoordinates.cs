﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct TileCoordinates
{
    [SerializeField] private int x;
    public int X { get { return x; } }

    [SerializeField] private int y;
    public int Y { get { return y; } }

    public TileCoordinates(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public override string ToString()
    {
        return "(" +X.ToString() + ", " + Y.ToString() + ")";
    }
}
