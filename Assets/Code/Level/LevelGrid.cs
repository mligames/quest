﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid : MonoBehaviour
{
    public EditorLevelData Data;
    public TileCoordinates PlayerStartCoordinates;
    public TilePrefabPalette TilePrefabs;
    public Dictionary<TileCoordinates, LevelTile> Tiles;

    public int TileCount;
    protected void Awake()
    {
        Tiles = new Dictionary<TileCoordinates, LevelTile>();
    }
    private void Start()
    {

    }
    public void LoadLevel()
    {
        foreach (var item in Data.TileDataList)
        {
            Debug.Log(item.coordinates + " " + item.tileType + " " + item.wallType);
            LevelTile levelTile = Instantiate(TilePrefabs.GroundTilePrefab, transform);

            levelTile.gameObject.transform.position = new Vector3(item.coordinates.X, 0.0f, item.coordinates.Y) * TilePrefabs.TileScale;
            levelTile.Coordinates = item.coordinates;

            if (item.isWall)
            {
                levelTile.TileType = TileType.WALL;
                WallData wallData = null;
                switch (item.wallType)
                {
                    case WallType.NONE:
                        break;
                    case WallType.BAR:
                        wallData = Instantiate(TilePrefabs.WallBarPrefab, levelTile.transform);
                        break;
                    case WallType.CORNER:
                        wallData = Instantiate(TilePrefabs.WallCornerPrefab, levelTile.transform);
                        break;
                    case WallType.T:
                        wallData = Instantiate(TilePrefabs.WallTPrefab, levelTile.transform);
                        break;
                    case WallType.X:
                        wallData = Instantiate(TilePrefabs.WallXPrefab, levelTile.transform);
                        break;
                }

                if(wallData != null)
                {
                    wallData.WallType = item.wallType;
                    wallData.gameObject.transform.position = levelTile.WorldLocation;
                    wallData.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, item.wallYRotation, 0));
                }
            }
            else
            {
                levelTile.TileType = TileType.GROUND;
                levelTile.IsPlayerStart = item.isPlayerStart;
                levelTile.IsEnemyStart = item.isEnemyStart;
            }
            levelTile.gameObject.name = levelTile.ToString();
        }
        TileCount = Tiles.Count;
    }

    public LevelTile GetTile(TileCoordinates coordinates)
    {
        if (Tiles.TryGetValue(coordinates, out LevelTile gridTile))
        {
            return gridTile;
        }
        Debug.Log("Tile" + coordinates.ToString() + " Not Found");
        return null;
    }
    public LevelTile GetTile(float x, float y)
    {
        TileCoordinates coordinates = new TileCoordinates((int)x, (int)y);

        if (Tiles.TryGetValue(coordinates, out LevelTile gridTile))
        {
            return gridTile;
        }
        Debug.Log("Tile" + coordinates.ToString() + " Not Found");
        return null;
    }
    public LevelTile GetTileInDirection(MoveDirection moveDirection, LevelTile currentTile)
    {
        TileCoordinates coordinates;
        switch (moveDirection)
        {
            case MoveDirection.UP:
                coordinates = new TileCoordinates(currentTile.Coordinates.X, currentTile.Coordinates.Y + 1);
                break;
            case MoveDirection.DOWN:
                coordinates = new TileCoordinates(currentTile.Coordinates.X, currentTile.Coordinates.Y - 1);
                break;
            case MoveDirection.LEFT:
                coordinates = new TileCoordinates(currentTile.Coordinates.X - 1, currentTile.Coordinates.Y);
                break;
            case MoveDirection.RIGHT:
                coordinates = new TileCoordinates(currentTile.Coordinates.X + 1, currentTile.Coordinates.Y);
                break;
            default:
                coordinates = currentTile.Coordinates;
                break;
        }

        return GetTile(coordinates);
    }
}
