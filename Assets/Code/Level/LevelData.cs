﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class LevelData : ScriptableObject
{
    public TilePrefabPalette TilePrefabPalette;
    public Vector2 LevelSize;
    public List<EditorTileData> TileDataList;
    public void SaveLevelMap(Dictionary<TileCoordinates, EditorTile> tiles)
    {
        TileDataList = new List<EditorTileData>();
        foreach (KeyValuePair<TileCoordinates, EditorTile> kvp in tiles)
        {
            TileDataList.Add(kvp.Value.SaveData());
        }
    }
}
